package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	estado()
	fmt.Println("Iniciando")
	wg.Add(2)
	go func() {
		fmt.Println("Primero")
		wg.Done()
	}()
	go func() {
		fmt.Println("Segundo")
		wg.Done()
	}()
	estado()
	fmt.Println("Finaliza")
	wg.Wait()
	estado()
}

func estado() {
	fmt.Printf("\tNúmero de CPU: %v\n", runtime.NumCPU())
	fmt.Printf("\tNúmero de Gorutinas: %v\n", runtime.NumGoroutine())
}
