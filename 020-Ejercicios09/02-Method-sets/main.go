package main

import (
	"fmt"
)

type persona struct {
	Nombre string
}

func (p *persona) hablar() {
	fmt.Println("Hola soy ", p.Nombre)
}

type humano interface {
	hablar()
}

func Algo(p humano) {
	p.hablar()
}

func main() {
	p1 := persona{
		Nombre: "Juan",
	}
	Algo(&p1)
	p1.hablar()
}
