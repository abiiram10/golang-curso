package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	x := 100
	wg.Add(x)
	incremento := 0
	for r := 0; r < 100; r++ {
		go func() {
			v := incremento
			runtime.Gosched()
			v++
			incremento = v
			fmt.Println(incremento)
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println("El valor final es ", incremento)
}
