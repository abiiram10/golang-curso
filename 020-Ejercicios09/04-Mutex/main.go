package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var m sync.Mutex

	x := 100
	wg.Add(x)
	incremento := 0
	for r := 0; r < 100; r++ {
		go func() {
			m.Lock()
			v := incremento
			v++
			incremento = v
			fmt.Println(incremento)
			m.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println("El valor final es ", incremento)
}
