package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var wg sync.WaitGroup
	var incremento int64
	x := 100
	wg.Add(x)
	for r := 0; r < 100; r++ {
		go func() {
			atomic.AddInt64(&incremento, 1)
			fmt.Println(atomic.LoadInt64(&incremento))
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println("El valor final es ", atomic.LoadInt64(&incremento))
}
