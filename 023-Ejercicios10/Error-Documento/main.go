package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	for a := 0; a < 1000000000000; a++ {
		f, error := os.Create(strconv.Itoa(a) + "holi.txt")
		if error != nil {
			fmt.Println(error)
			return
		}
		defer f.Close()

		r := strings.NewReader("Hola mundo cruel")
		io.Copy(f, r)
		fmt.Println(a)
	}
}
