package main

import (
	"fmt"

	"Gitlab.com/abiiram10/golang-curso/024-Pruebas/01-PBT/perrito"
)

type canino struct {
	nombre string
	edad   int
}

func main() {
	fido := canino{
		nombre: "Fido",
		edad:   perrito.Edad(10),
	}
	fmt.Println(fido)
	fmt.Println(perrito.EdadDos(10))
}
