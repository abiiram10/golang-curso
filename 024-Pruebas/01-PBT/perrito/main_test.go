package perrito

import (
	"fmt"
	"testing"
)

//Para ejecutar las pruebas go test

func TestEdad(t *testing.T) {

	type perro struct {
		EdadH int
		Edadp int
	}

	perros := []perro{
		perro{1, 7},
		perro{2, 14},
		perro{3, 21},
		perro{4, 28},
	}

	for _, v := range perros {
		n := Edad(v.EdadH)
		if n != v.Edadp {
			t.Error("Esperaba: ", v.Edadp, " / Recibe ", n)
		}
	}

}

func TestEdadDos(t *testing.T) {
	type perro struct {
		EdadH int
		Edadp int
	}

	perros := []perro{
		perro{1, 7},
		perro{2, 14},
		perro{3, 21},
		perro{4, 28},
	}

	for _, v := range perros {
		n := EdadDos(v.EdadH)
		if n != v.Edadp {
			t.Error("Esperaba: ", v.Edadp, " / Recibe ", n)
		}
	}
}

/* Ejemplos de las funciones
Mostrar ejemplos en el navegador
godoc -http=:8080

*/

func ExampleEdad() {
	fmt.Println(Edad(10))
	//Output:
	//70
}

func ExampleEdadDos() {
	fmt.Println(EdadDos(10))
	//Output:
	//70
}

//Para los Bench: go test -bench .

func BenchmarkEdad(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Edad(10)
	}
}

func BenchmarkEdadDos(b *testing.B) {
	for i := 0; i < b.N; i++ {
		EdadDos(10)
	}
}

/*

Covertura de las pruebas
	Muestra solo en consola
		go test -cover

	Mostrado en el navegador
		1.- Crear el archivo de covertura: go test -coverprofile c.out
		2.- Mostrarlo en navegador: go tool cover -html=c.out
*/
